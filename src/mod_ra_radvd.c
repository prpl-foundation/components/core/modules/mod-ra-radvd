/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/

#include <stdio.h>
#include <string.h>

#include "mod_ra_radvd.h"
#include "radvd_process.h"
#include "radvd_configuration.h"
#include "radvd_info_list.h"

#include <amxp/amxp.h>
#include <amxm/amxm.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

/**********************************************************
* Macro definitions
**********************************************************/

#define str_empty(x) ((x == NULL) || (*x == 0))

/* The minimum amount of time in ms to wait before updating /etc/radvd.conf and restarting
   the radvd daemon. This is to avoid needlessly restarting radvd too many times if multiple configuration
   changes follow each other in rapid succession.*/
#define RECONFIG_TIMEOUT 250

/* The maximum amount of times the reconfiguration and restarting of the radvd daemon can be delayed.
   If there are more than MAX_NR_RECONFIG_TIMEOUTS, with each two consecutive changes no more than RECONFIG_TIMEOUT
   ms apart, we update the configuration anyway and restart the daemon. */
#define MAX_NR_RECONFIG_TIMEOUTS 10
/**********************************************************
* Type definitions
**********************************************************/

struct _mod_ra_radvd {
    amxm_shared_object_t* so;
    amxp_timer_t* reconfig_timer;
    uint32_t nr_reconfig_delays;
};

/**********************************************************
* Variable declarations
**********************************************************/

static struct _mod_ra_radvd radvd;

/**********************************************************
* Function Prototypes
**********************************************************/

static void trigger_reconfig_timer(void);
static void reconfig_daemon(amxp_timer_t* timer, void* priv);

/**********************************************************
* Functions
**********************************************************/

static void radvd_intf_changed_cb(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  void* const priv) {
    SAH_TRACEZ_IN(ME);
    radvd_info_t* info = (radvd_info_t*) priv;
    char* intf_name = NULL;

    when_null_trace(info, exit, ERROR, "no radvd_info_t found in the private data");
    when_null_trace(data, exit, ERROR, "Invalid data parameter");

    intf_name = amxc_var_dyncast(cstring_t, data);
    if(str_empty(intf_name) && str_empty(info->intf_name)) {
        goto exit;
    }
    if(!str_empty(intf_name) && !str_empty(info->intf_name) && (strcmp(info->intf_name, intf_name) == 0)) {
        goto exit;
    }

    free(info->intf_name);
    info->intf_name = intf_name;
    intf_name = NULL;

    trigger_reconfig_timer();

exit:
    free(intf_name);
    SAH_TRACEZ_OUT(ME);
    return;
}

static int radvd_update_is(UNUSED const char* function_name,
                           amxc_var_t* args,
                           UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    radvd_info_t* info = NULL;
    SAH_TRACEZ_INFO(ME, "got a radvd update amxm call");

    info = radvd_info_list_get(GET_CHAR(args, "Interface"));
    if(info == NULL) {
        status = radvd_info_add(args, radvd_intf_changed_cb);
        when_failed_trace(status, exit, ERROR, "Failed to add info to the list");
    } else {
        status = radvd_info_update(args, info);
        when_failed_trace(status, exit, ERROR, "Failed to update info to the list");

        if(GET_BOOL(info->args, "Force")) {
            SAH_TRACEZ_INFO(ME, "Generating radvd config file");
            status = generate_radvd_config();
            when_failed_trace(status, exit, ERROR, "Failed to generate the new radvd config file");
            SAH_TRACEZ_INFO(ME, "Updating process %d", radvd_info_one_intf_enabled());
            status = radvd_process_update(radvd_info_one_intf_enabled());
            when_failed_trace(status, exit, ERROR, "Failed to update the radvd process");

        } else {
            trigger_reconfig_timer();
        }
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return (int) status;
}

static int radvd_remove_is(UNUSED const char* function_name,
                           amxc_var_t* args,
                           UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    const char* intf_path = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    SAH_TRACEZ_INFO(ME, "got a radvd remove amxm call");

    intf_path = GET_CHAR(args, "Interface");
    when_null_trace(intf_path, exit, ERROR, "Could not find the interface path");

    status = radvd_info_list_delete(intf_path);
    when_failed_trace(status, exit, ERROR, "Failed to delete intf_path %s from the list", intf_path);

    trigger_reconfig_timer();
exit:
    SAH_TRACEZ_OUT(ME);
    return (int) status;
}

static int radvd_shutdown(UNUSED const char* function_name,
                          UNUSED amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    SAH_TRACEZ_INFO(ME, "got a radvd shutdown amxm call");

    status = radvd_process_update(false);

    SAH_TRACEZ_OUT(ME);
    return (int) status;
}

static void trigger_reconfig_timer(void) {
    SAH_TRACEZ_INFO(ME, "Reconfigure timer triggered");
    when_null_trace(radvd.reconfig_timer, exit, ERROR, "Reconfig timer is NULL");

    if(radvd.nr_reconfig_delays == 0) {
        SAH_TRACEZ_WARNING(ME, "radvd reconfiguration delayed for %u times, forcing reconfiguration now", MAX_NR_RECONFIG_TIMEOUTS);
        amxp_timer_stop(radvd.reconfig_timer);
        radvd.nr_reconfig_delays = MAX_NR_RECONFIG_TIMEOUTS;
        reconfig_daemon(NULL, NULL);
    } else {
        radvd.nr_reconfig_delays--;
        amxp_timer_start(radvd.reconfig_timer, RECONFIG_TIMEOUT);
    }
exit:
    return;
}

static void reconfig_daemon(UNUSED amxp_timer_t* timer, UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "Reconfiguring daemon");
    amxd_status_t status = amxd_status_unknown_error;

    status = generate_radvd_config();
    when_failed_trace(status, exit, ERROR, "Failed to generate the new radvd config file");

    status = radvd_process_update(radvd_info_one_intf_enabled());
    when_failed_trace(status, exit, ERROR, "Failed to update the radvd process");

exit:
    return;
}

static AMXM_CONSTRUCTOR ra_radvd_start(void) {
    SAH_TRACEZ_IN(ME);
    amxm_module_t* mod = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    SAH_TRACEZ_INFO(ME, "ra_radvd start");

    radvd_info_list_init();
    status = radvd_process_init();
    when_failed_trace(status, exit, ERROR, "Failed to init radvd process");
    radvd.so = amxm_so_get_current();
    when_failed_trace(amxp_timer_new(&radvd.reconfig_timer, reconfig_daemon, NULL), exit, ERROR,
                      "Failed to create reconfigure timer");
    radvd.nr_reconfig_delays = MAX_NR_RECONFIG_TIMEOUTS;
    amxm_module_register(&mod, radvd.so, MOD_RA_RADVD);
    amxm_module_add_function(mod, "mod-update", radvd_update_is);
    amxm_module_add_function(mod, "mod-remove", radvd_remove_is);
    amxm_module_add_function(mod, "mod-shutdown", radvd_shutdown);
exit:
    SAH_TRACEZ_OUT(ME);
    return 0;
}

static AMXM_DESTRUCTOR ra_radvd_stop(void) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_INFO(ME, "ra_radvd stop");

    amxm_so_close(&radvd.so);
    radvd.so = NULL;
    amxp_timer_delete(&radvd.reconfig_timer);
    radvd.reconfig_timer = NULL;
    radvd_process_clean();
    radvd_info_list_clean();

    SAH_TRACEZ_OUT(ME);
    return 0;
}
