/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the ** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
   patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/

#include "radvd_configuration.h"
#include "radvd_info_list.h"
#include "mod_ra_radvd.h"

#include <amxb/amxb.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

/**********************************************************
* Macro definitions
**********************************************************/

#define RA_DVD_CONFIG_FILE "/etc/radvd.conf"
#define RA_DVD_CONFIG_FILE_TMP "/etc/radvd.conf.tmp"

/**********************************************************
* Type definitions
**********************************************************/

/**********************************************************
* Variable declarations
**********************************************************/

/**********************************************************
* Function Prototypes
**********************************************************/

/**********************************************************
* Functions
**********************************************************/

static amxd_status_t handle_prefixes(FILE* file_ptr, amxc_var_t* args) {
    SAH_TRACEZ_IN(ME);
    int ret = -1;
    amxd_status_t status = amxd_status_unknown_error;
    amxc_llist_t prefix_list;
    amxc_llist_t valid_lifetimes_list;
    amxc_llist_t preferred_lifetimes_list;
    amxc_llist_t autonomous_list;
    amxc_llist_t on_link_list;
    amxc_string_t prefixes_string;
    amxc_string_t valid_lifetimes_string;
    amxc_string_t preferred_lifetimes_string;
    amxc_string_t autonomous_string;
    amxc_string_t on_link_string;
    amxc_llist_it_t* prefix_it;
    amxc_llist_it_t* vl_it;
    amxc_llist_it_t* pl_it;
    amxc_llist_it_t* autonomous_it;
    amxc_llist_it_t* on_link_it;

    amxc_string_init(&prefixes_string, 0);
    amxc_string_init(&valid_lifetimes_string, 0);
    amxc_string_init(&preferred_lifetimes_string, 0);
    amxc_string_init(&autonomous_string, 0);
    amxc_string_init(&on_link_string, 0);

    amxc_llist_init(&prefix_list);
    amxc_llist_init(&valid_lifetimes_list);
    amxc_llist_init(&preferred_lifetimes_list);
    amxc_llist_init(&autonomous_list);
    amxc_llist_init(&on_link_list);

    when_null_trace(args, exit, ERROR, "Invalid input parameter prefixes");
    when_null_trace(file_ptr, exit, ERROR, "Invalid input parameter file_ptr");

    amxc_string_set(&prefixes_string, GET_CHAR(args, "ConvertedPrefixes"));
    amxc_string_set(&valid_lifetimes_string, GET_CHAR(args, "RelativeValidLifetimes"));
    amxc_string_set(&preferred_lifetimes_string, GET_CHAR(args, "RelativePreferredLifetimes"));
    amxc_string_set(&autonomous_string, GET_CHAR(args, "Autonomous"));
    amxc_string_set(&on_link_string, GET_CHAR(args, "OnLink"));

    ret = amxc_string_split_to_llist(&prefixes_string, &prefix_list, ',');
    when_failed_trace(ret, exit, ERROR, "Failed to convert char %s to llist", GET_CHAR(args, "ConvertedPrefixes"));
    ret = amxc_string_split_to_llist(&valid_lifetimes_string, &valid_lifetimes_list, ',');
    when_failed_trace(ret, exit, ERROR, "Failed to convert char %s to llist", GET_CHAR(args, "RelativeValidLifetimes"));
    ret = amxc_string_split_to_llist(&preferred_lifetimes_string, &preferred_lifetimes_list, ',');
    when_failed_trace(ret, exit, ERROR, "Failed to convert char %s to llist", GET_CHAR(args, "RelativePreferredLifetimes"));
    ret = amxc_string_split_to_llist(&autonomous_string, &autonomous_list, ',');
    when_failed_trace(ret, exit, ERROR, "Failed to convert char %s to llist", GET_CHAR(args, "Autonomous"));
    ret = amxc_string_split_to_llist(&on_link_string, &on_link_list, ',');
    when_failed_trace(ret, exit, ERROR, "Failed to convert char %s to llist", GET_CHAR(args, "OnLink"));

    prefix_it = amxc_llist_get_first(&prefix_list);
    vl_it = amxc_llist_get_first(&valid_lifetimes_list);
    pl_it = amxc_llist_get_first(&preferred_lifetimes_list);
    autonomous_it = amxc_llist_get_first(&autonomous_list);
    on_link_it = amxc_llist_get_first(&on_link_list);

    while(prefix_it != NULL && vl_it != NULL && pl_it != NULL && autonomous_it != NULL && on_link_it != NULL) {
        const char* prefix = amxc_string_get(amxc_container_of(prefix_it, amxc_string_t, it), 0);
        const char* valid_lifetime = amxc_string_get(amxc_container_of(vl_it, amxc_string_t, it), 0);
        const char* preferred_lifetime = amxc_string_get(amxc_container_of(pl_it, amxc_string_t, it), 0);
        const char* autonomous = amxc_string_get(amxc_container_of(autonomous_it, amxc_string_t, it), 0);
        const char* on_link = amxc_string_get(amxc_container_of(on_link_it, amxc_string_t, it), 0);

        if(str_empty(prefix) || str_empty(valid_lifetime) || str_empty(preferred_lifetime) || str_empty(autonomous) || str_empty(on_link)) {
            SAH_TRACEZ_ERROR(ME, "Invalid value in prefix data");
        } else {
            fprintf(file_ptr, "\n\tprefix %s {\n", prefix);
            fprintf(file_ptr, "\t\tAdvOnLink %s;\n", strcmp(on_link, "true") == 0 ? "on" : "off");
            fprintf(file_ptr, "\t\tAdvAutonomous %s;\n", strcmp(autonomous, "true") == 0 ? "on" : "off");
            if((strcmp(valid_lifetime, "0") != 0) && (strcmp(preferred_lifetime, "0") != 0)) {
                fprintf(file_ptr, "\t\tDecrementLifetimes on;\n");
            }
            fprintf(file_ptr, "\t\tDeprecatePrefix on;\n");
            fprintf(file_ptr, "\t\tAdvValidLifetime %s;\n", valid_lifetime);
            fprintf(file_ptr, "\t\tAdvPreferredLifetime %s;\n", preferred_lifetime);
            fprintf(file_ptr, "\t};\n");
        }

        prefix_it = amxc_llist_it_get_next(prefix_it);
        vl_it = amxc_llist_it_get_next(vl_it);
        pl_it = amxc_llist_it_get_next(pl_it);
        autonomous_it = amxc_llist_it_get_next(autonomous_it);
        on_link_it = amxc_llist_it_get_next(on_link_it);
    }

    status = amxd_status_ok;

exit:
    amxc_llist_clean(&prefix_list, amxc_string_list_it_free);
    amxc_llist_clean(&valid_lifetimes_list, amxc_string_list_it_free);
    amxc_llist_clean(&preferred_lifetimes_list, amxc_string_list_it_free);
    amxc_llist_clean(&autonomous_list, amxc_string_list_it_free);
    amxc_llist_clean(&on_link_list, amxc_string_list_it_free);
    amxc_string_clean(&prefixes_string);
    amxc_string_clean(&valid_lifetimes_string);
    amxc_string_clean(&preferred_lifetimes_string);
    amxc_string_clean(&autonomous_string);
    amxc_string_clean(&on_link_string);
    SAH_TRACEZ_OUT(ME);
    return status;
}

static amxd_status_t handle_deprecated_prefixes(FILE* file_ptr, amxc_var_t* prefixes) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t* prefix_list = NULL;

    when_null_trace(prefixes, exit, ERROR, "Invalid input parameter prefixes");
    when_null_trace(file_ptr, exit, ERROR, "Invalid input parameter file_ptr");

    prefix_list = GET_ARG(prefixes, "DeprecatedPrefixes");

    amxc_var_for_each(prefix, prefix_list) {
        fprintf(file_ptr, "\n\tprefix %s {\n", GET_CHAR(prefix, NULL));
        fprintf(file_ptr, "\t\tAdvOnLink on;\n");
        fprintf(file_ptr, "\t\tAdvAutonomous on;\n");
        fprintf(file_ptr, "\t\tAdvValidLifetime 0;\n");
        fprintf(file_ptr, "\t\tAdvPreferredLifetime 0;\n");
        fprintf(file_ptr, "\t};\n");
    }
    status = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

static amxd_status_t write_interface_config_to_file(radvd_info_t* radvd_info, void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    FILE* file_ptr = (FILE*) priv;
    amxc_string_t flag;
    amxc_string_t amx_string;
    SAH_TRACEZ_INFO(ME, "try to update the radvd config file");

    amxc_string_init(&amx_string, 0);
    amxc_string_init(&flag, 0);

    when_null_trace(radvd_info, exit, ERROR, "Invalid input parameter radvd_info");
    when_null_trace(file_ptr, exit, ERROR, "Invalid input parameter file_ptr");
    when_false_status(radvd_info->enabled && !str_empty(radvd_info->intf_name), exit, status = amxd_status_ok);

    fprintf(file_ptr, "interface %s {\n", radvd_info->intf_name);
    fprintf(file_ptr, "\tAdvSendAdvert on;\n");
    fprintf(file_ptr, "\tAdvIntervalOpt on;\n");

    if(GET_ARG(radvd_info->args, "MaxRtrAdvInterval") != NULL) {
        fprintf(file_ptr, "\tMaxRtrAdvInterval %d;\n", GET_UINT32(radvd_info->args, "MaxRtrAdvInterval"));
    }

    if(GET_ARG(radvd_info->args, "MinRtrAdvInterval") != NULL) {
        fprintf(file_ptr, "\tMinRtrAdvInterval %d;\n", GET_UINT32(radvd_info->args, "MinRtrAdvInterval"));
    }

    if(GET_ARG(radvd_info->args, "AdvDefaultLifetime") != NULL) {
        fprintf(file_ptr, "\tAdvDefaultLifetime %d;\n", GET_UINT32(radvd_info->args, "AdvDefaultLifetime"));
    }

    if(GET_ARG(radvd_info->args, "AdvLinkMTU") != NULL) {
        fprintf(file_ptr, "\tAdvLinkMTU %d;\n", GET_UINT32(radvd_info->args, "AdvLinkMTU"));
    }

    if(GET_ARG(radvd_info->args, "AdvReachableTime") != NULL) {
        fprintf(file_ptr, "\tAdvReachableTime %d;\n", GET_UINT32(radvd_info->args, "AdvReachableTime"));
    }

    if(GET_ARG(radvd_info->args, "AdvRetransTimer") != NULL) {
        fprintf(file_ptr, "\tAdvRetransTimer %d;\n", GET_UINT32(radvd_info->args, "AdvRetransTimer"));
    }

    if(GET_ARG(radvd_info->args, "AdvCurHopLimit") != NULL) {
        fprintf(file_ptr, "\tAdvCurHopLimit %d;\n", GET_UINT32(radvd_info->args, "AdvCurHopLimit"));
    }

    if(GET_BOOL(radvd_info->args, "AdvManagedFlag")) {
        fprintf(file_ptr, "\tAdvManagedFlag on;\n");
    }

    if(GET_BOOL(radvd_info->args, "AdvOtherConfigFlag")) {
        fprintf(file_ptr, "\tAdvOtherConfigFlag on;\n");
    }

    if(GET_BOOL(radvd_info->args, "AdvMobileAgentFlag")) {
        fprintf(file_ptr, "\tAdvMobRtrSupportFlag on;\n");
        fprintf(file_ptr, "\tAdvHomeAgentFlag on;\n");
        fprintf(file_ptr, "\tAdvHomeAgentInfo on;\n");
    }

    amxc_string_set(&flag, GET_CHAR(radvd_info->args, "AdvPreferredRouterFlag"));
    if(!amxc_string_is_empty(&flag)) {
        amxc_string_to_lower(&flag);
        if(strcmp(amxc_string_get(&flag, 0), "medium") != 0) {
            fprintf(file_ptr, "\tAdvDefaultPreference %s;\n", amxc_string_get(&flag, 0));
        }
    }

    status = handle_prefixes(file_ptr, radvd_info->args);
    when_failed_trace(status, exit, ERROR, "Failed to parse the prefix list");

    status = handle_deprecated_prefixes(file_ptr, radvd_info->args);
    when_failed_trace(status, exit, ERROR, "Failed to parse the deprecated prefix list");

    amxc_string_set(&amx_string, GET_CHAR(radvd_info->args, "RDNSS"));
    if(!amxc_string_is_empty(&amx_string)) {
        amxc_string_replace(&amx_string, ",", " ", UINT32_MAX);
        fprintf(file_ptr, "\n\tRDNSS %s {\n\t\tAdvRDNSSLifetime %u;\n\t};\n",
                amxc_string_get(&amx_string, 0),
                GET_UINT32(radvd_info->args, "AdvRDNSSLifetime"));
    }

    amxc_string_set(&amx_string, GET_CHAR(radvd_info->args, "DNSSL"));
    if(!amxc_string_is_empty(&amx_string)) {
        amxc_string_replace(&amx_string, ",", " ", UINT32_MAX);
        fprintf(file_ptr, "\n\tDNSSL %s {\n\t\tAdvDNSSLLifetime %u;\n\t};\n",
                amxc_string_get(&amx_string, 0),
                GET_UINT32(radvd_info->args, "AdvDNSSLLifetime"));
    }

    fprintf(file_ptr, "};\n\n");
    SAH_TRACEZ_INFO(ME, "Successfully updated the radvd config file");
exit:
    amxc_string_clean(&amx_string);
    amxc_string_clean(&flag);
    SAH_TRACEZ_OUT(ME);
    return status;
}

static amxd_status_t print_warning_message(FILE* file_ptr) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(file_ptr, exit, ERROR, "Invalid input parameter file_ptr");

    fprintf(file_ptr, "###################################################################################################\n");
    fprintf(file_ptr, "###  WARNING: This is a generated file, do not edit this file as changes will be overwritten.   ###\n");
    fprintf(file_ptr, "###           This file is generated by the module mod_ra-radvd,                                ###\n");
    fprintf(file_ptr, "###           from the tr181-routeradvertisement plugin.                                        ###\n");
    fprintf(file_ptr, "###################################################################################################\n\n");

    status = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t generate_radvd_config(void) {
    SAH_TRACEZ_IN(ME);
    FILE* file_ptr = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    file_ptr = fopen(RA_DVD_CONFIG_FILE_TMP, "w+");
    when_null_trace(file_ptr, exit, ERROR, "Could not create a new empty file");

    status = print_warning_message(file_ptr);
    when_failed_trace(status, exit, ERROR, "Failed to print the warning message in the radvd config file");

    status = radvd_info_list_exec_for_all(write_interface_config_to_file, file_ptr);
    when_failed_trace(status, exit, ERROR, "Failed to go over every intf in the info_list");

    fclose(file_ptr);
    status = (amxd_status_t) rename(RA_DVD_CONFIG_FILE_TMP, RA_DVD_CONFIG_FILE);
    when_failed_trace(status, exit, ERROR, "Failed to go rename file %s to %s", RA_DVD_CONFIG_FILE_TMP, RA_DVD_CONFIG_FILE);

    status = (amxd_status_t) chmod(RA_DVD_CONFIG_FILE, 0444); // allow read permission to the owner, group and other
    when_failed_trace(status, exit, ERROR, "Failed to change the permission of file %s", RA_DVD_CONFIG_FILE);
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}
