/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/

#include "radvd_process.h"

#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

/**********************************************************
* Macro definitions
**********************************************************/

/**********************************************************
* Type definitions
**********************************************************/

typedef enum radvd_status {
    RADVD_START,
    RADVD_STARTING,
    RADVD_STOP,
    RADVD_STOPPING,
    RADVD_RESTART
} radvd_status_t;

/**********************************************************
* Variable declarations
**********************************************************/

static radvd_status_t radvd_actual_status = RADVD_STOP;
static radvd_status_t radvd_status = RADVD_STOP;
static amxp_subproc_t* radvd_process = NULL;

/**********************************************************
* Function Prototypes
**********************************************************/

static amxd_status_t update_radvd_process_state(void);

/**********************************************************
* Functions
**********************************************************/

static void radvd_process_signal_handler(const char* const sig_name,
                                         const amxc_var_t* const data,
                                         UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    SAH_TRACEZ_INFO(ME, "Radvd was signalled %s", sig_name);

    if((GET_ARG(data, "Signalled") == NULL) || !GET_BOOL(data, "Signalled")) {
        SAH_TRACEZ_ERROR(ME, "radvd died with ExitCode[%d]. Check if the radvd.conf file is valid", GET_UINT32(data, "ExitCode"));
        radvd_actual_status = RADVD_STOP;
        //Do not update and rety to setup the radvd config (first fix the problem)
    } else if(strcmp(sig_name, "stop") == 0) {
        radvd_actual_status = RADVD_STOP;
        status = update_radvd_process_state();
        if(status != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to handle an radvd event");
        }
    } else {
        SAH_TRACEZ_ERROR(ME, "Unknown radvd event %s -> should never get here", sig_name);
    }
    SAH_TRACEZ_OUT(ME);
}

static amxd_status_t start_radvd_process(void) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    const char* cmd[] = {"/usr/sbin/radvd", "-d", "1", "-n", NULL};
    int ret = -1;

    when_null_trace(radvd_process, exit, ERROR, "the radvd process was not correctly initialized");
    when_false_trace(!amxp_subproc_is_running(radvd_process), exit, ERROR, "the radvd process is already running");

    ret = amxp_subproc_vstart(radvd_process, (char**) cmd);
    when_false_trace(ret == 0, exit, ERROR, "Failed to start the radvd subproc");
    SAH_TRACEZ_INFO(ME, "Radvd process was started");

    radvd_actual_status = RADVD_START;
    status = update_radvd_process_state();
    when_failed_trace(status, exit, ERROR, "Failed to update the radvd state, after starting radvd");
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

static amxd_status_t send_signal_radvd_process(int signal) {
    SAH_TRACEZ_IN(ME);
    int ret = -1;
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(radvd_process, exit, ERROR, "radvd process is not correctly initialized");
    when_false_trace(amxp_subproc_is_running(radvd_process), exit, ERROR, "trying to stop the subproc, but the subproc is not running");

    ret = amxp_subproc_kill(radvd_process, signal);
    when_false_trace(ret == 0, exit, ERROR, "Failed to signal the subproc with %d %s (%d)", signal, strerror(errno), errno);
    SAH_TRACEZ_INFO(ME, "Radvd process was signalled %d", signal);

    status = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

static amxd_status_t update_radvd_process_state(void) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_ok;

    when_true(radvd_status == RADVD_START && (radvd_actual_status == RADVD_START || radvd_actual_status == RADVD_STARTING), exit); // In correct state
    when_true(radvd_status == RADVD_STOP && (radvd_actual_status == RADVD_STOP || radvd_actual_status == RADVD_STOPPING), exit);   // In correct state
    when_true((radvd_actual_status == RADVD_STOPPING || radvd_actual_status == RADVD_STARTING), exit);                             // Radvd process is already busy, wait

    if(radvd_status == RADVD_START) {
        radvd_actual_status = RADVD_STARTING;
        status = start_radvd_process();
        when_failed_trace(status, exit, ERROR, "Failed to start the radvd process");
    } else if(radvd_status == RADVD_STOP) {
        radvd_actual_status = RADVD_STOPPING;
        status = send_signal_radvd_process(SIGTERM);
        if(status != amxd_status_ok) {
            SAH_TRACEZ_WARNING(ME, "Failed to stop radvd gracefully, forcefully kill it instead");
            status = send_signal_radvd_process(SIGKILL);
            when_failed_trace(status, exit, ERROR, "Failed to stop the radvd process");
        }
    } else if(radvd_status == RADVD_RESTART) {
        radvd_status = RADVD_START;
        radvd_actual_status = RADVD_STARTING;
        status = send_signal_radvd_process(SIGHUP);//reconfigure the radvd process
        when_failed_trace(status, exit, ERROR, "Failed to restart the radvd process");
        radvd_actual_status = RADVD_START;
    } else {
        status = amxd_status_unknown_error;
        SAH_TRACEZ_ERROR(ME, "Unknown state -> should never get here");
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t radvd_process_update(bool enable) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_INFO(ME, "Update the Radvd process %d", enable);
    amxd_status_t status = amxd_status_unknown_error;
    if(enable) {
        if((radvd_actual_status == RADVD_START) || (radvd_actual_status == RADVD_STARTING)) {
            radvd_status = RADVD_RESTART;
        } else {
            radvd_status = RADVD_START;
        }

        status = update_radvd_process_state();
        when_failed_trace(status, exit, ERROR, "failed to start/restart the radvd process");
    } else {
        radvd_status = RADVD_STOP;

        status = update_radvd_process_state();
        when_failed_trace(status, exit, ERROR, "failed to stop the radvd process");
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t radvd_process_init(void) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    int ret = -1;

    amxp_subproc_new(&radvd_process);

    ret = amxp_slot_connect(amxp_subproc_get_sigmngr(radvd_process), "*", NULL, radvd_process_signal_handler, NULL);
    when_false_trace(ret == 0, exit, ERROR, "Failed to connect the signal handler to the process");
    status = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

void radvd_process_clean(void) {
    SAH_TRACEZ_IN(ME);
    int ret = -1;
    amxd_status_t status = amxd_status_unknown_error;

    when_false_trace(amxp_subproc_is_running(radvd_process), exit, ERROR, "trying to stop the subproc, but the subproc is not running");

    ret = amxp_slot_disconnect(amxp_subproc_get_sigmngr(radvd_process), "*", radvd_process_signal_handler);
    if(ret != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to disconnect the amxp slot, on cleanup");
    }

    status = radvd_process_update(false);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Could not stop the radvd process, on cleanup");
    }

    amxp_subproc_wait(radvd_process, 2);
    if(amxp_subproc_is_running(radvd_process) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to stop the radvd process, on cleanup");
    }
    radvd_actual_status = RADVD_STOP;
exit:
    amxp_subproc_delete(&radvd_process);
    SAH_TRACEZ_OUT(ME);
}
