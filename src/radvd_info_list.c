/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "radvd_info_list.h"

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

/**********************************************************
* Macro definitions
**********************************************************/

/**********************************************************
* Type definitions
**********************************************************/

/**********************************************************
* Variable declarations
**********************************************************/

static amxc_llist_t* info_list = NULL;

/**********************************************************
* Function Prototypes
**********************************************************/

static void radvd_info_list_delete_item(amxc_llist_it_t* it);

/**********************************************************
* Functions
**********************************************************/

radvd_info_t* radvd_info_list_get(const char* intf) {
    SAH_TRACEZ_IN(ME);
    radvd_info_t* ret = NULL;
    radvd_info_t* info = NULL;

    when_null_trace(intf, exit, ERROR, "Bad input parameter intf");

    amxc_llist_for_each(it, info_list) {
        info = amxc_container_of(it, radvd_info_t, it);
        if(strcmp(intf, info->intf_path) == 0) {
            ret = info;
            break;
        }
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return ret;
}

amxd_status_t radvd_info_list_delete(const char* intf) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    radvd_info_t* info = NULL;

    when_null_trace(intf, exit, ERROR, "Bad input parameter intf");

    amxc_llist_for_each(it, info_list) {
        info = amxc_container_of(it, radvd_info_t, it);
        if(strcmp(intf, info->intf_path) == 0) {
            radvd_info_list_delete_item(it);
            status = amxd_status_ok;
            break;
        }
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

bool radvd_info_one_intf_enabled(void) {
    SAH_TRACEZ_IN(ME);
    radvd_info_t* info = NULL;
    bool enabled = false;

    amxc_llist_for_each(it, info_list) {
        info = amxc_container_of(it, radvd_info_t, it);
        if(info->enabled && !str_empty(info->intf_name)) {
            enabled = true;
            goto exit;
        }
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return enabled;
}

amxd_status_t radvd_info_add(amxc_var_t* args, netmodel_callback_t handler) {
    SAH_TRACEZ_IN(ME);
    radvd_info_t* info = NULL;
    const char* intf_path = NULL;
    int ret = -1;
    amxc_var_t var;
    amxd_status_t status = amxd_status_unknown_error;

    amxc_var_init(&var);

    when_null_trace(args, exit, ERROR, "Bad input parameter args");
    when_null_trace(handler, exit, ERROR, "Bad input parameter handler");

    intf_path = GET_CHAR(args, "Interface");
    when_str_empty_trace(intf_path, exit, ERROR, "Could not find the interface path");

    info = (radvd_info_t*) calloc(1, sizeof(radvd_info_t));
    when_null_trace(info, exit, ERROR, "Could not find the alias");

    ret = amxc_llist_append(info_list, &(info->it));
    if(ret != 0) {
        free(info);
        SAH_TRACEZ_ERROR(ME, "Failed to add the new info inst to the list");
        goto exit;
    }

    amxc_var_new(&info->args);
    amxc_var_move(info->args, args);
    info->enabled = (GET_ARG(info->args, "Enable") == NULL || GET_BOOL(info->args, "Enable"));
    info->intf_path = strdup(intf_path);

    info->nm_query = netmodel_openQuery_getFirstParameter(intf_path, "RouterAdvertisement", "NetDevName", "netdev-bound", netmodel_traverse_down, handler, (void*) info);
    if(info->nm_query == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to create a netmodel query for %s", intf_path);
        radvd_info_list_delete(intf_path);
        goto exit;
    }
    status = amxd_status_ok;
exit:
    amxc_var_clean(&var);
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t radvd_info_update(amxc_var_t* args, radvd_info_t* info) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(args, exit, ERROR, "Bad input parameter args");
    when_null_trace(info, exit, ERROR, "Bad input parameter info");

    amxc_var_move(info->args, args);
    info->enabled = GET_BOOL(info->args, "Enable");
    status = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t radvd_info_list_exec_for_all(radvd_cb_fnc_t func, void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_ok;

    when_null_trace(func, exit, ERROR, "Invalid input parameter func");
    when_null_trace(priv, exit, ERROR, "Invalid input parameter priv");

    amxc_llist_for_each(it, info_list) {
        radvd_info_t* info = amxc_container_of(it, radvd_info_t, it);
        status = func(info, priv);
        when_failed_trace(status, exit, ERROR, "Failed to execute function with intf_path %s", info->intf_path);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

static void radvd_info_list_delete_item(amxc_llist_it_t* it) {
    SAH_TRACEZ_IN(ME);
    radvd_info_t* info = NULL;

    when_null_trace(it, exit, ERROR, "Bad input parameter it");

    info = amxc_container_of(it, radvd_info_t, it);
    amxc_llist_it_take(it);

    amxc_var_delete(&(info->args));

    free(info->intf_path);
    info->intf_path = NULL;

    free(info->intf_name);
    info->intf_name = NULL;

    if(info->nm_query != NULL) {
        netmodel_closeQuery(info->nm_query);
        info->nm_query = NULL;
    }

    free(info);
    info = NULL;
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void radvd_info_list_init(void) {
    SAH_TRACEZ_IN(ME);
    when_false_trace(netmodel_initialize() == 1, exit, ERROR, "Failed to init netmodel");
    if(info_list == NULL) {
        amxc_llist_new(&info_list);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void radvd_info_list_clean(void) {
    SAH_TRACEZ_IN(ME);
    amxc_llist_delete(&info_list, radvd_info_list_delete_item);
    netmodel_cleanup();
    SAH_TRACEZ_OUT(ME);
}
