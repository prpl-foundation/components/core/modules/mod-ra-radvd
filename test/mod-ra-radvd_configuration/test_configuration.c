/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>


#include "test_mocks.h"
#include "test_configuration.h"
#include "radvd_configuration.h"
#include "radvd_info_list.h"

/**********************************************************
* Function Prototypes
**********************************************************/
extern FILE* fopen(const char* filename, const char* mode);
extern FILE* __real_fopen(const char* filename, const char* mode);
FILE* __wrap_fopen(const char* filename, const char* mode);
int __wrap_rename(const char* old_filename, const char* new_filename);
int __wrap_chmod(const char* pathname, mode_t mode);

/**********************************************************
* Functions
**********************************************************/

int test_setup(UNUSED void** state) {
    return 0;
}

int test_teardown(UNUSED void** state) {
    return 0;
}


static void radvd_intf_changed_cb(UNUSED const char* const sig_name,
                                  UNUSED const amxc_var_t* const data,
                                  UNUSED void* const priv) {
    radvd_info_t* info = (radvd_info_t*) priv;
    info->intf_name = amxc_var_dyncast(cstring_t, data);
    return;
}


static void add_radvd_info_instance(const char* interface) {
    amxc_var_t data;
    amxd_status_t status = amxd_status_ok;

    amxc_var_init(&data);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &data, "Interface", interface);
    amxc_var_add_key(cstring_t, &data, "Prefixes", "");
    amxc_var_add_key(cstring_t, &data, "RDNSS", "2001:db8:1::4,2001:db8:2::5");
    amxc_var_add_key(cstring_t, &data, "DNSSL", "example.com,com");
    amxc_var_add_key(cstring_t, &data, "AdvPreferredRouterFlag", "LOW");

    amxc_var_add_key(uint32_t, &data, "MinRtrAdvInterval", 1);
    amxc_var_add_key(uint32_t, &data, "MaxRtrAdvInterval", 2);
    amxc_var_add_key(uint32_t, &data, "AdvCurHopLimit", 3);
    amxc_var_add_key(uint32_t, &data, "AdvReachableTime", 4);
    amxc_var_add_key(uint32_t, &data, "AdvRetransTimer", 5);
    amxc_var_add_key(uint32_t, &data, "AdvDefaultLifetime", 6);
    amxc_var_add_key(uint32_t, &data, "AdvLinkMTU", 7);
    amxc_var_add_key(uint32_t, &data, "AdvRDNSSLifetime", 10001);
    amxc_var_add_key(uint32_t, &data, "AdvDNSSLLifetime", 10002);

    amxc_var_add_key(bool, &data, "AdvManagedFlag", 1);
    amxc_var_add_key(bool, &data, "AdvOtherConfigFlag", 1);
    amxc_var_add_key(bool, &data, "AdvMobileAgentFlag", 1);
    amxc_var_add_key(bool, &data, "Enable", 1);

    status = radvd_info_add(&data, radvd_intf_changed_cb);// add an instance
    assert_true(status == amxd_status_ok);

    amxc_var_clean(&data);
}

static void add_radvd_info_instance_with_default_values(const char* interface) {
    amxc_var_t data;
    amxd_status_t status = amxd_status_ok;

    amxc_var_init(&data);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &data, "Interface", interface);
    // The value in Prefixes should not matter, The ConvertedPrefixes, RelativePreferredLifetimes and RelativeValidLifetimes should be used instead
    amxc_var_add_key(cstring_t, &data, "Prefixes", "InvalidPrefix");
    amxc_var_add_key(cstring_t, &data, "RDNSS", "2001:db8:1::4,2001:db8:2::5");
    amxc_var_add_key(cstring_t, &data, "DNSSL", "example.com,com");
    amxc_var_add_key(cstring_t, &data, "AdvPreferredRouterFlag", "Medium");
    amxc_var_add_key(cstring_t, &data, "ConvertedPrefixes", "2a02:1802:94:33b1::/64,2a02:1802:94:33b2::/64");
    amxc_var_add_key(cstring_t, &data, "RelativePreferredLifetimes", "3600,1800");
    amxc_var_add_key(cstring_t, &data, "RelativeValidLifetimes", "1800,900");
    amxc_var_add_key(cstring_t, &data, "Autonomous", "true,true");
    amxc_var_add_key(cstring_t, &data, "OnLink", "true,true");

    amxc_var_add_key(uint32_t, &data, "MinRtrAdvInterval", 200);
    amxc_var_add_key(uint32_t, &data, "MaxRtrAdvInterval", 600);
    amxc_var_add_key(uint32_t, &data, "AdvCurHopLimit", 64);
    amxc_var_add_key(uint32_t, &data, "AdvReachableTime", 0);
    amxc_var_add_key(uint32_t, &data, "AdvRetransTimer", 0);
    amxc_var_add_key(uint32_t, &data, "AdvDefaultLifetime", 1800);
    amxc_var_add_key(uint32_t, &data, "AdvLinkMTU", 0);
    amxc_var_add_key(uint32_t, &data, "AdvRDNSSLifetime", 10001);
    amxc_var_add_key(uint32_t, &data, "AdvDNSSLLifetime", 10002);

    amxc_var_add_key(bool, &data, "AdvManagedFlag", 0);
    amxc_var_add_key(bool, &data, "AdvOtherConfigFlag", 0);
    amxc_var_add_key(bool, &data, "AdvMobileAgentFlag", 0);
    amxc_var_add_key(bool, &data, "Enable", 1);

    status = radvd_info_add(&data, radvd_intf_changed_cb);// add an instance
    assert_true(status == amxd_status_ok);

    amxc_var_clean(&data);
}

static int compareFile(const char* f1, const char* f2) {
    int rv = -1;
    char ch1;
    char ch2;

    FILE* fp1 = __real_fopen(f1, "r");
    FILE* fp2 = __real_fopen(f2, "r");

    do {
        ch1 = fgetc(fp1);
        ch2 = fgetc(fp2);
    } while(ch1 == ch2 && ch1 != EOF && ch2 != EOF);

    if((ch1 == EOF) && (ch2 == EOF)) {
        rv = 0;
    } else {
        char ln1[30];
        char ln2[30];
        if((fgets(ln1, 30, fp1) != NULL) && (fgets(ln2, 30, fp2) != NULL)) {
            ln1[strcspn(ln1, "\r\n")] = 0;
            ln2[strcspn(ln2, "\r\n")] = 0;
            printf("%s != %s\n", ln1, ln2);
        } else {
            printf("Files differ but can't show where");
        }
    }

    fclose(fp1);
    fclose(fp2);
    fflush(stdout);
    return rv;
}

void test_configuration_update(UNUSED void** state) {
    const char* interface1 = "Device.IP.Interface.2.";
    const char* interface2 = "Device.IP.Interface.3.";
    amxc_var_t data;
    amxd_status_t status = amxd_status_unknown_error;

    amxc_var_init(&data);
    amxc_var_set(cstring_t, &data, "interface_name");

    //add and instance and see if it gets printed
    expect_function_call(__wrap_rename);
    expect_function_call(__wrap_chmod);
    add_radvd_info_instance(interface1);
    trigger_getFirstParameter_callback(&data);
    status = generate_radvd_config();
    assert_true(status == amxd_status_ok);
    assert_int_equal(compareFile("test_radvd_conf", "expected_output/test_output_1"), 0);

    //add and extra instance with default values and see if it gets printed
    expect_function_call(__wrap_rename);
    expect_function_call(__wrap_chmod);
    add_radvd_info_instance_with_default_values(interface2);
    amxc_var_set(cstring_t, &data, "interface_name2");
    trigger_getFirstParameter_callback(&data);
    status = generate_radvd_config();
    assert_true(status == amxd_status_ok);
    assert_int_equal(compareFile("test_radvd_conf", "expected_output/test_output_2"), 0);

    //remove the first instance and see if it gets printed
    expect_function_call(__wrap_rename);
    expect_function_call(__wrap_chmod);
    radvd_info_list_delete(interface1);
    status = generate_radvd_config();
    assert_true(status == amxd_status_ok);
    assert_int_equal(compareFile("test_radvd_conf", "expected_output/test_output_3"), 0);

    //print nothing
    expect_function_call(__wrap_rename);
    expect_function_call(__wrap_chmod);
    radvd_info_list_delete(interface2);
    status = generate_radvd_config();
    assert_true(status == amxd_status_ok);
    assert_int_equal(compareFile("test_radvd_conf", "expected_output/test_output_4"), 0);

    amxc_var_clean(&data);
}

/**********************************************************
* Mocks
**********************************************************/
FILE* __wrap_fopen(UNUSED const char* filename, UNUSED const char* mode) {
    return __real_fopen("test_radvd_conf", "w+");
}

int __wrap_rename(const char* old_filename, UNUSED const char* new_filename) {
    assert_non_null(old_filename);
    assert_non_null(new_filename);
    function_called();
    return 0;
}

int __wrap_chmod(UNUSED const char* pathname, UNUSED mode_t mode) {
    assert_non_null(pathname);
    function_called();
    return 0;
}