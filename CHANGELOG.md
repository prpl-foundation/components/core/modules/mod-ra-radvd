# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.6.3 - 2024-12-12(09:44:33 +0000)

### Other

- [router-advertisement] infinite while loop was detected during a code review

## Release v0.6.2 - 2024-12-03(11:00:19 +0000)

### Other

- - [routeradvertisement] A and L flags must be set by default

## Release v0.6.1 - 2024-11-21(12:40:50 +0000)

### Other

- [tr181-routeradvertisement] Implement support for manual prefixes

## Release v0.6.0 - 2024-04-09(10:02:23 +0000)

### New

- [IPv6][Server]Upon IPv6 deactivation, "Valid Lifetime" and "Preferred Lifetime" values in RA not equal to 0

## Release v0.5.0 - 2024-03-14(15:17:54 +0000)

### New

- [TR181-RA][TR181-IP][IPv6][Server]No RA is sent to LAN clients when deactivating IPv6

## Release v0.4.1 - 2023-06-19(15:26:23 +0000)

### Other

- Opensource component

## Release v0.4.0 - 2023-06-07(08:29:31 +0000)

### New

- [CDROUTER][IPv6] The prefix lifetime decrementation seems occuring with a delay

## Release v0.3.2 - 2023-05-11(09:01:37 +0000)

## Release v0.3.1 - 2023-05-09(12:23:42 +0000)

### Fixes

- [TR181 RouterAdvertisement] no new message send after parameter AdvMobileAgentFlag update

## Release v0.3.0 - 2023-03-21(14:07:44 +0000)

### New

- [TR181 routeradvertisement] Multiple RA messages sent during start

## Release v0.2.1 - 2023-03-02(08:56:21 +0000)

### Fixes

- Box sends 2 ICMPv6 RA when a RS is received on LAN

## Release v0.2.0 - 2023-01-20(12:52:08 +0000)

### New

- [prpl][tr181-routeradvertisment] implement radvd module

## Release v0.1.1 - 2023-01-06(12:22:42 +0000)

### Other

- Add runtime dependency on radvd

## Release v0.1.0 - 2022-12-02(12:47:09 +0000)

### New

- [prpl][tr181-routeradvertisment] add radvd module

